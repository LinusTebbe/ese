<?php

declare(strict_types=1);

namespace Storage\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200414163241 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE contacts (id INT UNSIGNED AUTO_INCREMENT NOT NULL, facility_id INT UNSIGNED DEFAULT NULL, firstname VARCHAR(200) NOT NULL, lastname VARCHAR(200) NOT NULL, position VARCHAR(200) NOT NULL, phonenumber VARCHAR(200) NOT NULL, faxnumber VARCHAR(200) NOT NULL, email VARCHAR(200) NOT NULL, created_at DATETIME NOT NULL, updated_at DATETIME DEFAULT NULL, INDEX IDX_33401573A7014910 (facility_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE contacts ADD CONSTRAINT FK_33401573A7014910 FOREIGN KEY (facility_id) REFERENCES facilities (id)');
        $this->addSql('ALTER TABLE facilities DROP FOREIGN KEY FK_ADE885D5F5B7AF75');
        $this->addSql('ALTER TABLE facilities ADD CONSTRAINT FK_ADE885D5F5B7AF75 FOREIGN KEY (address_id) REFERENCES addresses (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE facility_services DROP FOREIGN KEY FK_253158E6A7014910');
        $this->addSql('ALTER TABLE facility_services DROP FOREIGN KEY FK_253158E6ED5CA9E6');
        $this->addSql('ALTER TABLE facility_services ADD CONSTRAINT FK_253158E6A7014910 FOREIGN KEY (facility_id) REFERENCES facilities (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE facility_services ADD CONSTRAINT FK_253158E6ED5CA9E6 FOREIGN KEY (service_id) REFERENCES services (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE facility_tags DROP FOREIGN KEY FK_4313CED4A7014910');
        $this->addSql('ALTER TABLE facility_tags DROP FOREIGN KEY FK_4313CED4BAD26311');
        $this->addSql('ALTER TABLE facility_tags ADD CONSTRAINT FK_4313CED4A7014910 FOREIGN KEY (facility_id) REFERENCES facilities (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE facility_tags ADD CONSTRAINT FK_4313CED4BAD26311 FOREIGN KEY (tag_id) REFERENCES tags (id) ON DELETE CASCADE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE contacts');
        $this->addSql('ALTER TABLE facilities DROP FOREIGN KEY FK_ADE885D5F5B7AF75');
        $this->addSql('ALTER TABLE facilities ADD CONSTRAINT FK_ADE885D5F5B7AF75 FOREIGN KEY (address_id) REFERENCES addresses (id)');
        $this->addSql('ALTER TABLE facility_services DROP FOREIGN KEY FK_253158E6A7014910');
        $this->addSql('ALTER TABLE facility_services DROP FOREIGN KEY FK_253158E6ED5CA9E6');
        $this->addSql('ALTER TABLE facility_services ADD CONSTRAINT FK_253158E6A7014910 FOREIGN KEY (facility_id) REFERENCES facilities (id)');
        $this->addSql('ALTER TABLE facility_services ADD CONSTRAINT FK_253158E6ED5CA9E6 FOREIGN KEY (service_id) REFERENCES services (id)');
        $this->addSql('ALTER TABLE facility_tags DROP FOREIGN KEY FK_4313CED4A7014910');
        $this->addSql('ALTER TABLE facility_tags DROP FOREIGN KEY FK_4313CED4BAD26311');
        $this->addSql('ALTER TABLE facility_tags ADD CONSTRAINT FK_4313CED4A7014910 FOREIGN KEY (facility_id) REFERENCES facilities (id)');
        $this->addSql('ALTER TABLE facility_tags ADD CONSTRAINT FK_4313CED4BAD26311 FOREIGN KEY (tag_id) REFERENCES tags (id)');
    }
}
