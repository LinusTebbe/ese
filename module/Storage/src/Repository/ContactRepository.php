<?php

declare(strict_types=1);

namespace Storage\Repository;

use Doctrine\ORM\EntityRepository;
use Storage\Entity\Contact;

class ContactRepository extends EntityRepository
{
    public function delete(Contact $contact): void
    {
        $entityManager = $this->getEntityManager();
        $entityManager->remove($contact);
        $entityManager->flush($contact);
    }

    public function save(Contact $contact): void
    {
        $entityManager = $this->getEntityManager();
        $entityManager->persist($contact);
        $entityManager->flush($contact);
    }
}