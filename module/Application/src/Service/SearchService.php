<?php

declare(strict_types=1);

namespace Application\Service;

use Application\Model\Search\SearchModel;
use Application\Model\SearchResult\SearchResultCollection;
use Storage\Entity\Facility;
use Storage\Entity\TagGroup;
use Storage\Repository\FacilityRepository;
use Storage\Repository\TagGroupRepository;

class SearchService
{
    private FacilityRepository $facilityRepository;
    private TagGroupRepository $tagGroupRepository;

    public function __construct(
        FacilityRepository $facilityRepository,
        TagGroupRepository $tagGroupRepository
    ) {
        $this->facilityRepository = $facilityRepository;
        $this->tagGroupRepository = $tagGroupRepository;
    }

    public function search(SearchModel $searchModel): SearchResultCollection
    {
        return new SearchResultCollection($this->facilityRepository->findBySearchModel($searchModel));
    }

    /**
     * @return Facility[]
     */
    public function getFacilities(): array
    {
        /** @var Facility[] $facilities */
        $facilities = $this->facilityRepository->findAll();

        return $facilities;
    }

    public function getFacilityById(int $facilityId): ?Facility
    {
        /** @var Facility|null $facility */
        $facility = $this->facilityRepository->find($facilityId);

        return $facility;
    }

    public function getTagGroup(int $tagGroupId)
    {
        /** @var TagGroup $tagGroup */
        $tagGroup = $this->tagGroupRepository->find($tagGroupId);

        return $tagGroup;
    }

    /**
     * @return TagGroup[]
     */
    public function getTagGroups(): array
    {
        /** @var TagGroup[] $tagGroups */
        $tagGroups = $this->tagGroupRepository->findAll();

        return $tagGroups;
    }

}