<?php

declare(strict_types=1);

namespace Administration\Service;

use Laminas\Authentication\AuthenticationService;
use Laminas\Session\SessionManager;
use Psr\Container\ContainerInterface;

class AuthManagerServiceFactory
{
    public function __invoke(ContainerInterface $container): AuthManagerService
    {
        return new AuthManagerService(
            $container->get(AuthenticationService::class),
            $container->get(SessionManager::class)
        );
    }
}