<?php

declare(strict_types=1);

namespace Common\Helper;

class DayOfWeekHelper
{
    public static function getDayOfWeekString(int $dayOfWeek): string
    {
        switch ($dayOfWeek) {
            case 0:
                return 'Sonntag';
                break;
            case 1:
                return 'Montag';
                break;
            case 2:
                return 'Dienstag';
                break;
            case 3:
                return 'Mittwoch';
                break;
            case 4:
                return 'Donnerstag';
                break;
            case 5:
                return 'Freitag';
                break;
            default:
                return 'Samstag';
                break;
        }
    }
}