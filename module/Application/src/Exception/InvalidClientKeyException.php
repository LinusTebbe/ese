<?php

declare(strict_types=1);

namespace Application\Exception;

use Exception;

class InvalidClientKeyException extends Exception
{
}
