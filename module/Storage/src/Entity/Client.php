<?php

declare(strict_types=1);

namespace Storage\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(
 *     name="clients",
 *     options={"collate"="utf8mb4_unicode_ci", "charset"="utf8mb4"},
 *     uniqueConstraints={
 *        @ORM\UniqueConstraint(name="uq_client_key",
 *            columns={"client_key"})
 *    }
 * )
 * @ORM\Entity(repositoryClass="Storage\Repository\ClientRepository")
 */
class Client
{
    /**
     * @ORM\Column(name="id", type="integer", options={"unsigned": true})
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @ORM\Column(name="client_key", type="string", length=40)
     */
    private string $clientKey;

    /**
     * @ORM\Column(name="name", type="string", length=200)
     */
    private string $name;

    /**
     * @ORM\OneToMany(targetEntity="Facility", mappedBy="client")
     */
    private Collection $facilities;

    /**
     * @ORM\OneToMany(targetEntity="TagGroup", mappedBy="client")
     */
    private Collection $tagGroups;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getClientKey(): string
    {
        return $this->clientKey;
    }

    public function setClientKey(string $clientKey): self
    {
        $this->clientKey = $clientKey;

        return $this;
    }

    public function getFacilities(): Collection
    {
        return $this->facilities;
    }

    public function getTagGroups(): Collection
    {
        return $this->tagGroups;
    }
}
