<?php

namespace Storage\Migrations\Command;

class DiffCommand extends \Doctrine\Migrations\Tools\Console\Command\DiffCommand
{
    protected static $defaultName = 'migrations:diff';

    protected function getTemplate(): string
    {
        return file_get_contents(__DIR__ . '/MigrationDiffTemplate.tmpl');
    }


}