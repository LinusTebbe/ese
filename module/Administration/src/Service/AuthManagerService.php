<?php

declare(strict_types=1);

namespace Administration\Service;

use Laminas\Authentication\AuthenticationService;
use Laminas\Authentication\Result;
use Laminas\Session\SessionManager;

class AuthManagerService
{
    private AuthenticationService $authService;
    private SessionManager $sessionManager;

    public function __construct(
        AuthenticationService $authService,
        SessionManager $sessionManager
    ) {
        $this->authService = $authService;
        $this->sessionManager = $sessionManager;
    }

    public function login(string $email, string $password, bool $rememberMe)
    {
        if ($this->authService->getIdentity() !== null) {
            return new Result(
                Result::SUCCESS,
                $this->authService->getIdentity()
            );
        }

        /** @var AuthAdapter $authAdapter */
        $authAdapter = $this->authService->getAdapter();
        $authAdapter->setEmail($email);
        $authAdapter->setPassword($password);
        $result = $this->authService->authenticate();

        // If user wants to "remember him", we will make session to expire in
        // one month. By default session expires in 1 hour (as specified in our
        // config/global.php file).
        if ($result->getCode() === Result::SUCCESS && $rememberMe) {
            // Session cookie will expire in 1 month (30 days).
            $this->sessionManager->rememberMe(60 * 60 * 24 * 30);
        }

        return $result;
    }

    public function logout(): void
    {
        if ($this->authService->getIdentity()==null) {
            throw new \Exception('The user is not logged in');
        }

        $this->authService->clearIdentity();
    }
}