<?php

declare(strict_types=1);

namespace Application\Service;

use Storage\Entity\Tag;
use Storage\Repository\TagRepository;

class TagService
{
    private TagRepository $tagRepository;

    public function __construct(TagRepository $tagRepository)
    {
        $this->tagRepository = $tagRepository;
    }

    /**
     * @return Tag[]
     */
    public function getTags(): array
    {
        /** @var Tag[] $tags */
        $tags = $this->tagRepository->findAll();

        return $tags;
    }

    public function getTag(int $tagId): Tag
    {
        /** @var Tag $tag */
        $tag = $this->tagRepository->find($tagId);

        return $tag;
    }

    public function save(Tag $tag): void
    {
        $this->tagRepository->save($tag);
    }

    public function delete(Tag $tag): void
    {
        $this->tagRepository->delete($tag);
    }
}