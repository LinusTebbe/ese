<?php

declare(strict_types=1);

namespace Administration\Controller;

use Administration\Service\AuthManagerService;
use Laminas\Authentication\AuthenticationService;
use Laminas\Authentication\Result;
use Laminas\Http\Request;
use Laminas\Mvc\Controller\AbstractActionController;
use Laminas\View\Model\ViewModel;

class AuthController extends AbstractActionController
{
    private AuthManagerService $authManagerService;
    private AuthenticationService $authService;

    public function __construct(
        AuthManagerService $authManagerService,
        AuthenticationService $authService
    ) {
        $this->authManagerService = $authManagerService;
        $this->authService = $authService;
    }

    public function loginAction(): ViewModel
    {
        $layout = $this->layout();
        $layout->setTemplate('layout/login');

        $isLoginError = false;

        /** @var Request $request */
        $request = $this->getRequest();

        // Check if user has submitted the form
        if ($request->isPost()) {
            $email = $request->getPost('mailaddress');
            $password = $request->getPost('password');
            $rememberMe = (bool) $request->getPost('rememberMe');

            $result = $this->authManagerService->login($email, $password, $rememberMe);
            if ($result->getCode() === Result::SUCCESS) {
                $this->redirect()->toRoute('administration');
            } else {
                $isLoginError = true;
            }
        }

        return new ViewModel([
            'isLoginError' => $isLoginError
        ]);
    }

    /**
     * @throws \Exception
     */
    public function logoutAction()
    {
        $this->authManagerService->logout();

        return $this->redirect()->toRoute('administration/login');
    }
}