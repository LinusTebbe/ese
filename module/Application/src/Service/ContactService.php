<?php

declare(strict_types=1);

namespace Application\Service;

use Storage\Entity\Contact;
use Storage\Repository\ContactRepository;

class ContactService
{
    private ContactRepository $contactRepository;

    public function __construct(ContactRepository $contactRepository)
    {
        $this->contactRepository = $contactRepository;
    }

    public function delete(Contact $contact): void
    {
        $this->contactRepository->delete($contact);
    }

    public function save(Contact $contact): void
    {
        $this->contactRepository->save($contact);
    }

    public function getContact(int $contactId): ?Contact
    {
        /** @var Contact $contact */
        $contact = $this->contactRepository->find($contactId);

        return $contact;
    }
}