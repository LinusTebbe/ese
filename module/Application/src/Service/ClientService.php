<?php

declare(strict_types=1);

namespace Application\Service;

use Storage\Entity\Client;
use Storage\Repository\ClientRepository;

/**
 * @copyright CHECK24 Vergleichsportal Hotel GmbH
 */
class ClientService
{
    private ClientRepository $clientRepository;

    public function __construct(ClientRepository $clientRepository)
    {
        $this->clientRepository = $clientRepository;
    }

    public function findAllWithAtLeastOneFacility(): array
    {
        return $this->clientRepository->findAllWithAtLeastOneFacility();
    }

    public function findByClientKey(string $clientKey): ?Client
    {
        $client = $this->clientRepository->findByClientKey($clientKey);

        if (count($client) === 1) {
            return reset($client);
        }

        return null;
    }
}
