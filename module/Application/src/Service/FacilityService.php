<?php

declare(strict_types=1);

namespace Application\Service;

use Storage\Entity\Facility;
use Storage\Repository\FacilityRepository;

class FacilityService
{
    private FacilityRepository $facilityRepository;

    public function __construct(FacilityRepository $facilityRepository)
    {
        $this->facilityRepository = $facilityRepository;
    }

    public function getFacility(int $facilityId): Facility
    {
        return $this->facilityRepository->find($facilityId);
    }

    public function save(Facility $facility): void
    {
        $this->facilityRepository->save($facility);
    }

    public function delete(Facility $facility): void
    {
        $this->facilityRepository->delete($facility);
    }
}