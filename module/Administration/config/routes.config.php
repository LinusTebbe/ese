<?php

declare(strict_types=1);

namespace Administration;

use Administration\Controller\AuthController;
use Administration\Controller\AdministrationController;
use Laminas\Router\Http\Literal;
use Laminas\Router\Http\Segment;

return  [
    'administration' => [
        'type' => Literal::class,
        'options' => [
            'route' => '/administration',
            'constraints' => [
                'tld' => '[a-zA-Z]*',
            ],
            'defaults' => [
                'controller' => AdministrationController::class,
                'action' => 'index',
            ],
        ],
        'may_terminate' => true,
        'child_routes' => [
            'login' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/login',
                    'defaults' => [
                        'controller' => AuthController::class,
                        'action' => 'login',
                    ],
                ],
            ],
            'logout' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/logout',
                    'defaults' => [
                        'controller' => AuthController::class,
                        'action' => 'logout',
                    ],
                ],
            ],
            'facilities' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/facilities',
                    'defaults' => [
                        'action' => 'index',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'edit' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/:facilityId/edit',
                            'defaults' => [
                                'action' => 'facilitiesEdit',
                            ],
                        ],
                    ],
                    'add' => [
                        'type' => Literal::class,
                        'options' => [
                            'route' => '/add',
                            'defaults' => [
                                'action' => 'facilitiesAdd',
                            ],
                        ],
                    ],
                    'delete' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/:facilityId/delete',
                            'defaults' => [
                                'action' => 'facilitiesDelete',
                            ],
                        ],
                    ],
                    'contacts' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/:facilityId/contacts',
                            'defaults' => [
                                'action' => 'contacts',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'edit' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/:contactId/edit',
                                    'defaults' => [
                                        'action' => 'contactsEdit',
                                    ],
                                ],
                            ],
                            'add' => [
                                'type' => Literal::class,
                                'options' => [
                                    'route' => '/add',
                                    'defaults' => [
                                        'action' => 'contactsAdd',
                                    ],
                                ],
                            ],
                            'delete' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/:contactId/delete',
                                    'defaults' => [
                                        'action' => 'contactsDelete',
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
            'taggroups' => [
                'type' => Literal::class,
                'options' => [
                    'route' => '/taggroups',
                    'defaults' => [
                        'action' => 'taggroups',
                    ],
                ],
                'may_terminate' => true,
                'child_routes' => [
                    'add' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/add',
                            'defaults' => [
                                'action' => 'taggroupsAdd',
                            ],
                        ],
                    ],
                    'edit' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/:tagGroupId/edit',
                            'defaults' => [
                                'action' => 'taggroupsEdit',
                            ],
                        ],
                    ],
                    'delete' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/:tagGroupId/delete',
                            'defaults' => [
                                'action' => 'taggroupsDelete',
                            ],
                        ],
                    ],
                    'tags' => [
                        'type' => Segment::class,
                        'options' => [
                            'route' => '/:tagGroupId/tags',
                            'defaults' => [
                                'action' => 'tags',
                            ],
                        ],
                        'may_terminate' => true,
                        'child_routes' => [
                            'add' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/add',
                                    'defaults' => [
                                        'action' => 'tagsAdd',
                                    ],
                                ],
                            ],
                            'edit' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/:tagId/edit',
                                    'defaults' => [
                                        'action' => 'tagsEdit',
                                    ],
                                ],
                            ],
                            'delete' => [
                                'type' => Segment::class,
                                'options' => [
                                    'route' => '/:tagId/delete',
                                    'defaults' => [
                                        'action' => 'tagsDelete',
                                    ],
                                ],
                            ],
                        ]
                    ],
                ],
            ],
        ],
    ],
];