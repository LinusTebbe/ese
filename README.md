# Einrichtungssuche Essen

Live: https://ese.henrikthesing.de

## Local development

Starting the container.
```bash
docker-compose up -d
```

Creating a local `.env` file
```bash
cp .env.skel .env
```

Running composer install.
```bash
docker-compose exec ese-web composer install
```

Run migrations
```bash
docker-compose exec ese-web php public/index.php migrations:migrate
```

(Optional) Jump inside the container.
```bash
docker-compose exec ese-web bash
```
