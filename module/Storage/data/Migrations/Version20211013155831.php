<?php

declare(strict_types=1);

namespace Storage\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211013155831 extends AbstractMigration
{
    public function getDescription() : string
    {
        return 'Add client datasets';
    }

    public function up(Schema $schema) : void
    {
        $this->addSql(
            <<<'SQL'
            INSERT INTO clients (id, client_key, name) VALUES (1, 'essen', 'Essen'), (2, 'leipzig', 'Leipzig');
            SQL
        );
    }

    public function down(Schema $schema) : void
    {
    }
}
