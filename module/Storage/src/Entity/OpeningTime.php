<?php

declare(strict_types=1);

namespace Storage\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(
 *     name="facility_opening_times",
 *     options={"collate"="utf8mb4_unicode_ci", "charset"="utf8mb4"}
 * )
 * @ORM\Entity(repositoryClass="Storage\Repository\OpeningTimesRepository")
 */
class OpeningTime
{
    /**
     * @var int|null
     * @ORM\Column(name="id", type="integer", options={"unsigned": true})
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private ?int $id = null;

    /**
     * @ORM\ManyToOne(targetEntity="Facility", inversedBy="openingTimes", cascade={"remove"})
     * @ORM\JoinColumn(name="facility_id", referencedColumnName="id")
     */
    private Facility $facility;

    /**
     * @var int
     * @ORM\Column(name="day_of_week", type="integer")
     */
    private int $dayOfWeek;

    /**
     * @var \DateTime
     * @ORM\Column(name="start_time", type="time")
     */
    private \DateTime $startTime;

    /**
     * @var \DateTime
     * @ORM\Column(name="end_time", type="time")
     */
    private \DateTime $endTime;

    /**
     * @ORM\Column(name="period", type="string", length=2)
     */
    private string $period;

    public function __construct(
        Facility $facility,
        int $dayOfWeek,
        \DateTime $startTime,
        \DateTime $endTime,
        string $period
    ) {
        $this->facility = $facility;
        $this->dayOfWeek = $dayOfWeek;
        $this->startTime = $startTime;
        $this->endTime = $endTime;
        $this->period = $period;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFacility(): Facility
    {
        return $this->facility;
    }

    public function setFacility(Facility $facility): self
    {
        $this->facility = $facility;

        return $this;
    }

    public function getDayOfWeek(): int
    {
        return $this->dayOfWeek;
    }

    public function setDayOfWeek(int $dayOfWeek): self
    {
        $this->dayOfWeek = $dayOfWeek;

        return $this;
    }

    public function getStartTime(): \DateTime
    {
        return $this->startTime;
    }

    public function setStartTime(\DateTime $startTime): self
    {
        $this->startTime = $startTime;

        return $this;
    }

    public function getEndTime(): \DateTime
    {
        return $this->endTime;
    }

    public function setEndTime(\DateTime $endTime): self
    {
        $this->endTime = $endTime;

        return $this;
    }

    public function getPeriod(): string
    {
        return $this->period;
    }

    public function setPeriod(string $period): self
    {
        $this->period = $period;

        return $this;
    }
}