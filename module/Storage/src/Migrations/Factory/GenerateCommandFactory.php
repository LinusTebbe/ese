<?php

namespace Storage\Migrations\Factory;

use Doctrine\DBAL\Migrations\Configuration\Configuration;
use Psr\Container\ContainerInterface;
use Storage\Migrations\Command\GenerateCommand;

class GenerateCommandFactory
{
    /**
     * @param ContainerInterface $container
     * @param $requestedName
     * @param array|null $options
     * @return GenerateCommand
     */
    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    ): GenerateCommand {
        /** @var Configuration $configuration */
        $configuration = $container->get('doctrine.migrations_configuration.orm_default');
        $command = new GenerateCommand();
        $command->setMigrationConfiguration($configuration);

        return $command;
    }
}